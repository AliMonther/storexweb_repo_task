<?php


namespace App\DTO;


class GetResponsePaginationData
{

    public static function getResponsePaginationData($result,$caster){

        return new ResponsePaginationData(
            [
                'paginator' =>$result,
                'collection'=>$result,
                'caster'=>$caster
            ]
        );
    }
}
