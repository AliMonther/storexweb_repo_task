    $( "body" ).delegate( ".complete-task-check", "click", function() {
     var $task_id =$(this).attr('task-id');
     var $status = $(this).attr('status-id');
     var $new_status =0;
     if($status == "1"){
         $new_status =2 ;
     }
     else{
         $new_status =1;
     }
    console.log('status'+$new_status);
        $.ajax({
            type:'POST',
            url:'http://localhost:8081/api/v1/task-change-status/'+$task_id,
            headers: {"Authorization":'Bearer '+localStorage.getItem('token')},
            data:{
                type:"DefaultTaskChangeStatusAction",
                new_status :$new_status,
            },
            dataType: 'json',
            success:function (data) {
                console.log(data);
                if($new_status === 2){
                    $("#task-"+$task_id).css("text-decoration","line-through").css('color','green');

                }else{
                    $("#task-"+$task_id).css("text-decoration","none").css('color','grey');

                }

            },
            error: function ($err) {
                console.log($err);
            }

        });

    });