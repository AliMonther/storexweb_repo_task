
    $("#add-checklist-btn").click(function(){
        $("#new-checklist-modal").modal('show');
    });

    $( "body" ).delegate( "#send-checklist-btn", "click", function() {
        var $title = $("#title");
        var $description = $("#description");

        $.ajax({
            type:'POST',
            url:'http://localhost:8081/api/v1/checklists',
            headers: {"Authorization":'Bearer '+localStorage.getItem('token')},

            data:{
                type:"DefaultChecklistStoreAction",
                title :$title.val(),
                description : $description.val(),
            },
            dataType: 'json',
            success:function (data) {
                console.log(data);
              alert('checklist added successfully');
                //location.reload();

                $("#checklists-content").prepend('<div class="col-xs-6 col-md-6 col-lg-6 checklist-cart"  id="checklist-'+data.data.id+'">\n' +
                    '\n' +
                    '            <span data-toggle="modal" data-target="#exampleModal " id="task-viewer"  check-id="'+data.data.id+'" class="card-span">\n' +
                    '                       <h4 class="title">'+data.data.title+'</h4>\n' +
                    '                        <p class="description" style="overflow: visible;">'+data.data.description+'</p>\n' +
                    '            </span>\n' +
                    '\n' +
                    '            <input class="form-check-input complete-checkbox" type="checkbox" id="checklist-box-'+data.data.id+'" value="complete" >\n' +
                    '\n' +
                    '\n' +
                    '        </div>');
            },
            error: function ($err) {
                alert($err);
            }

        });
    });
