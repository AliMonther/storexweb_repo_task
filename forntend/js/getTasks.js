
$( "body" ).delegate( "#task-viewer", "click", function() {

    $(".add-new-task").hide();
    $tasks_list = $("#tasks-list");
    $tasks_list.empty();

    $("#tasks-modal").modal('show');

   $checklist_id = $(this).attr('check-id');
    $("#new-task-btn").attr('checklist-id',$checklist_id);
    $.ajax({
        type:'GET',
        url:'http://localhost:8081/api/v1/tasks?type=DefaultTaskIndexAction&items_per_page=10&checklist_id='+$checklist_id,
        headers: {"Authorization":'Bearer '+localStorage.getItem('token')},
        dataType: 'json',
        success:function (data) {
            console.log(data);
            if(data.data.length>0){
                $.each(data.data,function (i , item) {
                    $tasks_list.append('<li class="list-group-item disabled " id="task-'+item.id+'">\n' +
                        '                        <span class="label label-primary" >'+item.title+'</span>\n' +
                        '                        <span class="label label-primary" style="float: right">'+item.date+'</span>\n' +
                        '                    </li>\n'+
                        '  <input class="form-check-input complete-task-check" type="checkbox" status-id="'+item.status.id+'" task-id="'+item.id+'" value="" id="complete-task-'+item.id+'">'

                    );
                    if(item.status.id ===2){
                        $("#task-"+item.id).css("text-decoration","line-through").css('color','green');
                        $("#complete-task-"+item.id).prop("checked", true);

                    }

                });
            }else{
                $tasks_list.html('<p style="margin-left: 200px"></p>')
            }

        },
        error: function ($err) {
            alert($err);
        }

    });
});