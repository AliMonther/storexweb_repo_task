
$( "body" ).delegate( ".send-new-task-button", "click", function() {
    $checklist_id = $(this).attr('checklist-id');
    console.log($checklist_id);
    $task_name = $("#task-name").val();
    $task_date = $("#task-date").val();
    console.log($task_name);
    console.log($task_date);
    $(this).html('<div class="spinner-border text-primary loading-spinner" role="status">\n' +
        '  <span class="sr-only">Loading...</span>\n' +
        '</div>');
    $.ajax({
        type:'POST',
        url:'http://localhost:8081/api/v1/tasks',
        headers: {"Authorization":'Bearer '+localStorage.getItem('token')},
        dataType: 'json',
        data:{
            type:"DefaultTaskStoreAction",
            checklist_id: $checklist_id,
            title:$task_name,
            date:$task_date
        },
        success:function (data) {
            $(".loading-spinner").remove();

            console.log(data);
            $tasks_list.prepend('<li class="list-group-item disabled " id="task-'+data.data.id+'">\n' +
                '                        <span class="label label-primary" >'+data.data.title+'</span>\n' +
                '                        <span class="label label-primary" style="float: right">'+data.data.date+'</span>\n' +
                '                    </li>\n'+
                '  <input class="form-check-input complete-task-check" type="checkbox" status-id="'+data.data.status.id+'" task-id="'+data.data.id+'" value="" id="complete-task-'+data.data.id+'">'

            );
        },
        error: function ($err) {
            alert($err);
        }

    });


});