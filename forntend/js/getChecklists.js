$.ajax({
    type:'GET',
    url:'http://localhost:8081/api/v1/checklists?type=DefaultChecklistIndexAction&items_per_page=100',
    headers: {"Authorization":'Bearer '+localStorage.getItem('token')},
    success:function (data ) {
        console.log(data);
        $.each(data.data,function (i , item) {
            $checklists.append('<div class="col-xs-6 col-md-6 col-lg-6 checklist-cart"  id="checklist-'+item.id+'">\n' +
                '\n' +
                '            <span data-toggle="modal" data-target="#exampleModal " check-id="'+item.id+'" id="task-viewer" class="card-span">\n' +
                '                       <h4 class="title">'+item.title+'</h4>\n' +
                '                        <p class="description" style="overflow: visible;">'+item.description+'</p>\n' +
                '            </span>\n' +
                '\n' +
                '            <input class="form-check-input complete-checkbox" type="checkbox" status="'+item.status+'" checklist-id="'+item.id+'" id="checklist-box-'+item.id+'" value="complete" >\n' +
                '\n' +
                '\n' +
                '        </div>');
            if(item.status !== "TODO"){
                $("#checklist-"+item.id).addClass('complete-checklist-card');
                $("#checklist-box-"+item.id).prop("checked", true);
                console.log(  $("#checklist-box-"+item.id));
            }
        });
    },
    error: function (err) {
        console.log(err);
    }

});

