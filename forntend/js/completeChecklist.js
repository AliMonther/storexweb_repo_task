$( "body" ).delegate('.complete-checkbox','click',function () {

    $new_status =0;
    $checklist_id = $(this).attr('checklist-id');
    $old_status = $(this).attr('status');

    if($old_status =="TODO"){
        $new_status = 2;
    }
    else{
        $new_status = 1;
    }
    $.ajax({
        type:'POST',
        url:'http://localhost:8081/api/v1/checklist-change-status/'+$checklist_id,
        headers: {"Authorization":'Bearer '+localStorage.getItem('token')},
        data:{
            type:"DefaultChecklistChangeStatusAction",
            new_status :$new_status,
        },
        dataType: 'json',
        success:function (data) {
            console.log(data);
            if($new_status === 2){
                $("#checklist-"+$checklist_id).addClass("complete-checklist-card");

            }else{

            }

        },
        error: function ($err) {
            console.log($err);
        }

    });


});