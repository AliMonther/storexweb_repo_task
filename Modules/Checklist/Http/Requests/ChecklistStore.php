<?php

namespace Modules\Checklist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChecklistStore extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>['required','string'],
            'title'=>['required','string'],
            'description'=>['required','string'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
