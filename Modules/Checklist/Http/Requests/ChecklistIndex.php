<?php

namespace Modules\Checklist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChecklistIndex extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>['required','string'],
            'items_per_page'=>['required','integer'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
