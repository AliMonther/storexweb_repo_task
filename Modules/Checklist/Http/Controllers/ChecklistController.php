<?php

namespace Modules\Checklist\Http\Controllers;

use App\DTO\GetResponseData;
use App\DTO\ResponseData;
use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Checklist\Actions\ChecklistChangeStatusAction\ChecklistChangeStatusFactory;
use Modules\Checklist\Actions\ChecklistIndexAction\ChecklistIndexFactory;
use Modules\Checklist\Actions\ChecklistStoreAction\ChecklistStoreFactory;
use Modules\Checklist\Actions\ChecklistUpdateAction\ChecklistUpdateFactory;
use Modules\Checklist\DTO\ChecklistDTO\ChecklistData;
use Modules\Checklist\Entities\Checklist;
use Modules\Checklist\Http\Requests\ChecklistChangeStatus;
use Modules\Checklist\Http\Requests\ChecklistIndex;
use Modules\Checklist\Http\Requests\ChecklistStore;
use Modules\Checklist\Http\Requests\ChecklistUpdate;

class ChecklistController extends Controller
{
        /**
         * Display a listing of the resource.
         * @param ChecklistIndex $request
         * @return void
         */
    public function index(ChecklistIndex $request)
    {
        return (new ChecklistIndexFactory($request->type , $request->items_per_page))->executeFactory();
    }


    /**
     * Store a newly created resource in storage.
     * @param ChecklistStore $request
     * @return Renderable
     */
    public function store(ChecklistStore $request)
    {
        $checklist_data = ChecklistData::fromRequest($request);
        return (new ChecklistStoreFactory($request->type , $checklist_data))->executeFactory();
    }

        /**
         * Show the specified resource.
         * @param Checklist $checklist
         * @return ResponseData
         */
    public function show(Checklist $checklist)
    {
        return GetResponseData::getResponseData(ChecklistData::fromModel($checklist->load('tasks')) , null , 200);
    }

    /**
     * Update the specified resource in storage.
     * @param ChecklistUpdate $request
     * @param Checklist $checklist
     * @return void
     */
    public function update(ChecklistUpdate $request, Checklist $checklist)
    {
        $checklist_data = ChecklistData::fromRequest($request);
        return (new ChecklistUpdateFactory($request->type , $checklist_data , $checklist))->executeFactory();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param ChecklistChangeStatus $request
     * @param Checklist $checklist
     */
    public function changeStatus(ChecklistChangeStatus $request , Checklist $checklist){

        return (new ChecklistChangeStatusFactory($request->type ,  $checklist , $request->new_status))->executeFactory();
    }

    public function getCompletedChecklistsPercentage(){

        $checklists_count = DB::table('checklists')->count();
        $completed_checklists = DB::table('checklists')->where('status_id',2)->count();
        $per =( $completed_checklists /$checklists_count)*100;
        return response()->json([
           'data'=>(int)$per,
           'message'=>null,
           'status'=>200
        ],200);
    }
}
