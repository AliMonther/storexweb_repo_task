<?php

namespace Modules\Checklist\Http\Controllers;

use App\DTO\GetResponseData;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Checklist\Actions\TaskChangeStatusAction\TaskChangeStatusFactory;
use Modules\Checklist\Actions\TaskIndexAction\TaskIndexFactory;
use Modules\Checklist\Actions\TaskStoreAction\TaskStoreFactory;
use Modules\Checklist\DTO\TaskDTO\TaskData;
use Modules\Checklist\Entities\Task;
use Modules\Checklist\Http\Requests\TaskChangeStatus;
use Modules\Checklist\Http\Requests\TaskIndex;
use Modules\Checklist\Http\Requests\TaskStore;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param TaskIndex $request
     * @return Renderable
     */
    public function index(TaskIndex $request)
    {
        return (new TaskIndexFactory($request->type , $request->items_per_page , $request->checklist_id))->executeFactory();
    }

    /**
     * Store a newly created resource in storage.
     * @param TaskStore $request
     * @return Renderable
     */
    public function store(TaskStore $request)
    {
        $task_data = TaskData::fromRequest($request);
        return (new TaskStoreFactory($request->type , $task_data))->executeFactory();
    }

    /**
     * Show the specified resource.
     * @param Task $task
     * @return \App\DTO\ResponseData
     */
    public function show(Task $task)
    {
        return GetResponseData::getResponseData(TaskData::fromModel($task->load('checklist')),null,200);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param Task $task
     * @return \App\DTO\ResponseData
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return GetResponseData::getResponseData(null , 'task deleted successfully' , 200);
    }

    public function changeStatus(TaskChangeStatus $request , Task $task){
        return (new TaskChangeStatusFactory($request->type ,$request->new_status , $task))->executeFactory();
    }
}
