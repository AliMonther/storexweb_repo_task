<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Checklist\Http\Controllers\ChecklistController;
use Modules\Checklist\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/checklist', function (Request $request) {
    return $request->user();
});


Route::prefix('v1')->group(function() {

    Route::middleware('auth')->group(function(){
        Route::apiResource('checklists', 'ChecklistController');
        Route::apiResource('tasks', 'TaskController');
        Route::post('checklist-change-status/{checklist}', [ChecklistController::class ,'changeStatus']);
        Route::post('task-change-status/{task}', [TaskController::class ,'changeStatus']);
        Route::get('checklists-per', [ChecklistController::class ,'getCompletedChecklistsPercentage']);
    });

});
