<?php

namespace Modules\Checklist\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Status extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Checklist\Database\factories\StatusFactory::new();
    }
    public function Checklists(){
        return $this->hasMany(Checklist::class);
    }


    public function tasks(){
        return $this->hasMany(Task::class);
    }

}
