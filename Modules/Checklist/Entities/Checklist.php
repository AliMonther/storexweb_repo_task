<?php

namespace Modules\Checklist\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\User\Entities\User;
use phpDocumentor\Reflection\Utils;

class Checklist extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'description',
        'created_at',
        'user_id',
        'status_id',
    ];

    protected static function newFactory()
    {
        return \Modules\Checklist\Database\factories\ChecklistFactory::new();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tasks(){
        return $this->hasMany(Task::class);
    }

    public  function status(){
        return $this->belongsTo(Status::class);
    }
}
