<?php

namespace Modules\Checklist\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'checklist_id',
        'status_id',
        'title',
        'date',
    ];

    protected static function newFactory()
    {
        return \Modules\Checklist\Database\factories\TaskFactory::new();
    }

    public function checklist(){
        return $this->belongsTo(Checklist::class);
    }

    public function status(){
        return $this->belongsTo(Status::class);
    }

}
