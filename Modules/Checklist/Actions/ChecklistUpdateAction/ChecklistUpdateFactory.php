<?php

namespace Modules\Checklist\Actions\ChecklistUpdateAction;

class ChecklistUpdateFactory
{

       private $class_name;
       private $checklist_data ;
       private $checklist;

       public function __construct($type  ,$checklist_data ,$checklist){


            $this->checklist_data = $checklist_data;
            $this->checklist = $checklist;
           $this->class_name = "Modules\\Checklist\\Actions\\ChecklistUpdateAction\\" .$type;
       }

       public function executeFactory(){
           $factor =   new $this->class_name();
           $result = $factor->execute($this->checklist_data , $this->checklist);
           return $factor->getResponse($result);
       }
}
