<?php

namespace Modules\Checklist\Actions\ChecklistUpdateAction;


use App\DTO\GetResponseData;
use Modules\Checklist\DTO\ChecklistDTO\ChecklistData;
use Modules\Checklist\Entities\Checklist;

class DefaultChecklistUpdateAction extends ChecklistUpdateAction
{


    public function execute(ChecklistData $checklist_data, Checklist $checklist)
    {
        $arr_data = $checklist_data->toArray();
        $checklist->update($arr_data);
        $checklist->save();

        return $checklist;
    }

    public function getResponse($result)
    {
        return GetResponseData::getResponseData(ChecklistData::fromModel($result) , 'checklist updated successfully' ,200);
    }
}
