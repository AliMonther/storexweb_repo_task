<?php

namespace Modules\Checklist\Actions\ChecklistUpdateAction;

use Modules\Checklist\DTO\ChecklistDTO\ChecklistData;
use Modules\Checklist\Entities\Checklist;

abstract class ChecklistUpdateAction
{
    abstract public function execute(ChecklistData $checklist_data , Checklist $checklist);
    abstract public function getResponse($result);

}
