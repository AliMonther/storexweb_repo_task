<?php

namespace Modules\Checklist\Actions\TaskIndexAction;


use App\DTO\GetResponsePaginationData;
use Modules\Checklist\DTO\ChecklistDTO\ChecklistCaster;
use Modules\Checklist\DTO\TaskDTO\TaskCaster;
use Modules\Checklist\Entities\Checklist;
use Modules\Checklist\Entities\Task;
use Modules\GlobalActions\BuildQuery;

class DefaultTaskIndexAction extends TaskIndexAction
{


    public function execute($checklist_id ,$items_per_page)
    {
        $builder = new BuildQuery();
        $result = $builder->execute('tasks',Task::class);
        $result = $result->where('checklist_id',$checklist_id);
        return $builder->getWithPagination($result,$items_per_page);

    }

    public function getResponse($result)
    {
        return GetResponsePaginationData::getResponsePaginationData(
            $result ,
            (new TaskCaster())->cast($result)
        );

    }
}
