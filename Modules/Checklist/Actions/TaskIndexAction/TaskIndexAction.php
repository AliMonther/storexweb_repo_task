<?php

namespace Modules\Checklist\Actions\TaskIndexAction;

abstract class TaskIndexAction
{
    abstract public function execute($checklist_id,$items_per_page);
    abstract public function getResponse($result);
}
