<?php

namespace Modules\Checklist\Actions\TaskIndexAction;

class TaskIndexFactory
{

       private $class_name;
       private $items_par_page;
       private $checklist_id;

       public function __construct($type , $items_par_page , $checklist_id){

          $this->items_par_page = $items_par_page;
          $this->checklist_id = $checklist_id;
           $this->class_name = "Modules\\Checklist\\Actions\\TaskIndexAction\\" .$type;
       }

       public function executeFactory(){

           $factor =   new $this->class_name();
           $result = $factor->execute($this->checklist_id ,$this->items_par_page);
           return $factor->getResponse($result);

       }
}
