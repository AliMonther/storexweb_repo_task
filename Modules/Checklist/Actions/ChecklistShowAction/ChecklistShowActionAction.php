<?php

namespace Modules\Checklist\Actions\ChecklistShowAction;

abstract class ChecklistShowActionAction
{
    abstract public function execute();
}
