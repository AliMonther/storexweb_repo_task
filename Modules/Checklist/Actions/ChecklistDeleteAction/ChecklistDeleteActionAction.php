<?php

namespace Modules\Checklist\Actions\ChecklistDeleteAction;

abstract class ChecklistDeleteActionAction
{
    abstract public function execute();
}
