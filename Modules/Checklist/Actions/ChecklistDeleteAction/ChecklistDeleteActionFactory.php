<?php

namespace Modules\Checklist\Actions\ChecklistDeleteAction;

class ChecklistDeleteActionFactory
{

       private $class_name;

       public function __construct($type){
           $this->class_name = "Modules\\Module_name\\Actions\\...\\" .$type;
       }

       public function executeFactory(){
            return (new $this->class_name())->execute();
       }
}
