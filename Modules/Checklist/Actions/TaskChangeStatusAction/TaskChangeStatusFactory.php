<?php

namespace Modules\Checklist\Actions\TaskChangeStatusAction;

class TaskChangeStatusFactory
{

       private $class_name;
       private $new_status;
       private $task;

       public function __construct($type , $new_status , $task){

           $this->new_status = $new_status;
           $this->task = $task;
           $this->class_name = "Modules\\Checklist\\Actions\\taskChangeStatusAction\\" .$type;
       }

       public function executeFactory(){

           $factor =   new $this->class_name();
           $result = $factor->execute($this->task , $this->new_status );
           return $factor->getResponse($result);

       }
}
