<?php

namespace Modules\Checklist\Actions\TaskChangeStatusAction;


use App\DTO\GetResponseData;
use Modules\Checklist\Entities\Task;

class DefaultTaskChangeStatusAction extends TaskChangeStatusAction
{


    public function execute(Task $task ,$new_status)
    {
        $task->update(
            [
                'status_id'=>$new_status,
            ]
        );
    }

    public function getResponse($result)
    {
        return GetResponseData::getResponseData(null , 'task status changed' , 200);

    }
}
