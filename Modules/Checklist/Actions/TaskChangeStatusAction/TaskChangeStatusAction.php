<?php

namespace Modules\Checklist\Actions\TaskChangeStatusAction;

use Modules\Checklist\Entities\Task;

abstract class TaskChangeStatusAction
{
    abstract public function execute(Task $task ,$new_status);
    abstract public function getResponse($result);
}
