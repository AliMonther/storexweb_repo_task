<?php

namespace Modules\Checklist\Actions\ChecklistChangeStatusAction;

use Modules\Checklist\Entities\Checklist;

abstract class ChecklistChangeStatusAction
{
    abstract public function execute(Checklist $checklist , $new_status);
    abstract public function getResponse($result);
}
