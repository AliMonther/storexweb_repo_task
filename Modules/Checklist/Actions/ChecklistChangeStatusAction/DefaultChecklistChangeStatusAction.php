<?php

namespace Modules\Checklist\Actions\ChecklistChangeStatusAction;


use App\DTO\GetResponseData;
use Modules\Checklist\Entities\Checklist;
use Modules\Checklist\Entities\Task;

class DefaultChecklistChangeStatusAction extends ChecklistChangeStatusAction
{


    public function execute(Checklist $checklist , $new_status)
    {
        $checklist->status_id = $new_status;
        $checklist->save();

        if($new_status == 2){

            $tasks = $this->getChecklistTasks($checklist->id);
            foreach ($tasks as $task){
                $this->makeTaskComplete($task , $new_status);
            }

        }

        return true;
    }


    public function getChecklistTasks($checklist_id){

        return Task::where('checklist_id',$checklist_id)->pluck('id');
    }

    public function makeTaskComplete($task_id , $status_id){

        Task::find($task_id)->update(
            [
                'status_id'=>$status_id
            ]
        );
    }



    public function getResponse($result)
    {
       return GetResponseData::getResponseData(null , 'checklist status changed' , 200);
    }

}
