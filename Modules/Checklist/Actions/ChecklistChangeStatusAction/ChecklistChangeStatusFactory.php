<?php

namespace Modules\Checklist\Actions\ChecklistChangeStatusAction;

use Modules\Checklist\Entities\Checklist;

class ChecklistChangeStatusFactory
{

       private $class_name;
       private $checklist;
       private $new_status;

       public function __construct($type , Checklist $checklist , $new_status){

           $this->checklist = $checklist;
           $this->new_status = $new_status;
           $this->class_name = "Modules\\Checklist\\Actions\\ChecklistChangeStatusAction\\" .$type;
       }

       public function executeFactory(){


           $factor =   new $this->class_name();
           $result = $factor->execute($this->checklist , $this->new_status);
           return $factor->getResponse($result);
       }
}
