<?php

namespace Modules\Checklist\Actions\TaskStoreAction;


use App\DTO\GetResponseData;
use Modules\Checklist\DTO\TaskDTO\TaskData;
use Modules\Checklist\Entities\Task;

class DefaultTaskStoreAction extends TaskStoreAction
{


    public function execute(TaskData $task_data)
    {
        $arr_data = $task_data->toArray();

        $arr_data['status_id']=1;
        $task = new Task($arr_data);
        $task->save();

        return $task;

    }

    public function getResponse($result)
    {

        return GetResponseData::getResponseData(TaskData::fromModel($result),'task created successfully',200);
    }
}
