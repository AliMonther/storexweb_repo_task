<?php

namespace Modules\Checklist\Actions\TaskStoreAction;

class TaskStoreFactory
{

       private $class_name;
       private $task_data;

       public function __construct($type , $task_data){

           $this->task_data = $task_data;
           $this->class_name = "Modules\\Checklist\\Actions\\TaskStoreAction\\" .$type;
       }

       public function executeFactory(){
           $factor =   new $this->class_name();
           $result = $factor->execute($this->task_data);
           return $factor->getResponse($result);
       }
}
