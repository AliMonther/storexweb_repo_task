<?php

namespace Modules\Checklist\Actions\TaskStoreAction;

use Modules\Checklist\DTO\TaskDTO\TaskData;

abstract class TaskStoreAction
{
    abstract public function execute(TaskData $task_data);
    abstract public function getResponse($result);
}
