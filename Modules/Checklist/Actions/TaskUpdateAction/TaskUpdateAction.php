<?php

namespace Modules\Checklist\Actions\TaskUpdateAction;

abstract class TaskUpdateAction
{
    abstract public function execute();
}
