<?php

namespace Modules\Checklist\Actions\ChecklistIndexAction;

abstract class ChecklistIndexAction
{
    abstract public function execute($items_per_page);
    abstract public function getResponse($result);
}
