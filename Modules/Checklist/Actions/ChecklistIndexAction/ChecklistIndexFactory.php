<?php

namespace Modules\Checklist\Actions\ChecklistIndexAction;

class ChecklistIndexFactory
{

       private $class_name;
       private $items_per_page;

       public function __construct($type , $items_per_page){

           $this->items_per_page = $items_per_page;
           $this->class_name = "Modules\\Checklist\\Actions\\ChecklistIndexAction\\" .$type;
       }

       public function executeFactory(){
           $factor =   new $this->class_name();
           $result = $factor->execute($this->items_per_page);
           return $factor->getResponse($result);
       }
}
