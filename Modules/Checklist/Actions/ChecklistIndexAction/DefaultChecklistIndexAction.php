<?php

namespace Modules\Checklist\Actions\ChecklistIndexAction;


use App\DTO\GetResponseData;
use App\DTO\GetResponsePaginationData;
use Illuminate\Support\Facades\Auth;
use Modules\Checklist\DTO\ChecklistDTO\ChecklistCaster;
use Modules\Checklist\Entities\Checklist;
use Modules\GlobalActions\BuildQuery;

class DefaultChecklistIndexAction extends ChecklistIndexAction
{


    public function execute($items_per_page)
    {
        $builder = new BuildQuery();
        $result = $builder->execute('checklists',Checklist::class);
        $result = $result->where('user_id',Auth::id());
           return $builder->getWithPagination($result,$items_per_page);

    }

    public function getResponse($result)
    {
        return GetResponsePaginationData::getResponsePaginationData(
            $result ,
            (new ChecklistCaster())->cast($result)
        );


    }
}
