<?php

namespace Modules\Checklist\Actions\ChecklistStoreAction;

class ChecklistStoreFactory
{

       private $class_name;
        private $checklist_data;
       public function __construct($type , $checklist_data){

           $this->checklist_data = $checklist_data;
           $this->class_name = "Modules\\Checklist\\Actions\\ChecklistStoreAction\\" .$type;
       }

       public function executeFactory(){
            $factor =   new $this->class_name();
            $result = $factor->execute($this->checklist_data);
            return $factor->getResponse($result);
       }
}
