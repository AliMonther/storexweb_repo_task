<?php

namespace Modules\Checklist\Actions\ChecklistStoreAction;

use Modules\Checklist\DTO\ChecklistDTO\ChecklistData;

abstract class ChecklistStoreAction
{
    abstract public function execute(ChecklistData $checklist_data);
    abstract public function getResponse($result);
}
