<?php

namespace Modules\Checklist\Actions\ChecklistStoreAction;


use App\DTO\GetResponseData;
use Illuminate\Support\Facades\Auth;
use Modules\Checklist\DTO\ChecklistDTO\ChecklistData;
use Modules\Checklist\Entities\Checklist;

class DefaultChecklistStoreAction extends ChecklistStoreAction
{


    public function execute(ChecklistData $checklist_data)
    {

       $arr_data = $checklist_data->toArray();
       $arr_data['status_id'] = 1;
       $arr_data['user_id'] = Auth::id();

       $checklist = new Checklist($arr_data);
       $checklist->save();

       return $checklist;

    }

    public function getResponse($result)
    {
        if(is_object($result))
            return GetResponseData::getResponseData(ChecklistData::fromModel($result),'checklist created successfully',200);
        return GetResponseData::getResponseData(null,'Error creating checklist !!',500);
    }
}
