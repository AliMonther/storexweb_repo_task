<?php

declare(strict_types=1);

namespace Modules\Checklist\DTO\TaskDTO;

use Modules\Checklist\Entities\Task;
use Spatie\DataTransferObject\Caster;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class  TaskCaster implements Caster
{

public function cast(mixed $value): mixed
    {
        return $value->map(/**
         * @throws UnknownProperties
         */
            function(Task $model){
                return TaskData::fromModel($model);
            })->toArray();
    }

}
