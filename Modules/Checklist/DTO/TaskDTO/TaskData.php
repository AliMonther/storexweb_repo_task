<?php


namespace Modules\Checklist\DTO\TaskDTO;

use Illuminate\Http\Request;;

use Modules\Checklist\DTO\ChecklistDTO\ChecklistData;
use Modules\Checklist\DTO\StatusDTO\StatusData;
use Spatie\DataTransferObject\DataTransferObject;
use Modules\Checklist\Entities\Task;

class TaskData extends DataTransferObject
{

// Define All Variables Here....

    public $id;
    public $title;
    public $date;
    public $checklist_id;
    public $checklist;
    public $status;
    public static function fromModel(Task $model){
     $data =  [
         'id'=>$model->id,
         'title'=>$model->title,
         'checklist'=>($model->relationLoaded('checklist') && $model->checklist) ? ChecklistData::fromModel($model->checklist) : null,
         'date'=>$model->date,
         'status'=>$model->status ? StatusData::fromModel($model->status) : null ,
     ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;

    }


    public static function fromRequest(Request $request){


        $from_data = $request->validated();

        $data = [
            'checklist_id'=>array_key_exists('checklist_id',$from_data) ? $from_data['checklist_id'] : null,
            'title'=>array_key_exists('title',$from_data) ? $from_data['title'] : null,
            'date'=>array_key_exists('title',$from_data) ? $from_data['date'] : null,


        ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;
    }


}
