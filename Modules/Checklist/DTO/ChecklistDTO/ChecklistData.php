<?php


namespace Modules\Checklist\DTO\ChecklistDTO;

use Illuminate\Http\Request;
use Modules\Checklist\DTO\TaskDTO\TaskCaster;
use Modules\Checklist\DTO\TaskDTO\TaskData;
use Spatie\DataTransferObject\DataTransferObject;
use Modules\Checklist\Entities\Checklist;

class ChecklistData extends DataTransferObject
{

// Define All Variables Here....

    public $id;
    public $title;
    public $description;
    public $tasks;
    public $status;

    public static function fromModel(Checklist $model){
     $data =  [

         'id'=>$model->id,
         'title'=>$model->title,
         'tasks'=>($model->relationLoaded('tasks') && $model->tasks) ?
             (new TaskCaster())->cast($model->tasks) :
             null,
         'description'=>$model->description,
         'status'=>$model->status->name,
     ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;

    }


    public static function fromRequest(Request $request){

         $from_data = $request->validated();
          $data = [
              'title'=>array_key_exists('title',$from_data) ? $from_data['title'] : null,
              'description'=>array_key_exists('description',$from_data) ? $from_data['description'] : null,
          ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;
    }


}
