<?php

declare(strict_types=1);

namespace Modules\Checklist\DTO\ChecklistDTO;

use Modules\Checklist\Entities\Checklist;
use Spatie\DataTransferObject\Caster;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class  ChecklistCaster implements Caster
{

public function cast(mixed $value): mixed
    {
        return $value->map(/**
         * @throws UnknownProperties
         */
            function(Checklist $model){
                return ChecklistData::fromModel($model);
            })->toArray();
    }

}
