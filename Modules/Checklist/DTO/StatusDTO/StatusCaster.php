<?php

declare(strict_types=1);

namespace Modules\Checklist\DTO\StatusDTO;

use Spatie\DataTransferObject\Caster;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class  StatusCaster implements Caster
{

public function cast(mixed $value): mixed
    {
        return $value->map(/**
         * @throws UnknownProperties
         */
            function(Model $model){
                return DtoData::fromModel($model);
            })->toArray();
    }

}
