<?php


namespace Modules\Checklist\DTO\StatusDTO;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;
use Modules\Checklist\Entities\Status;

class StatusData extends DataTransferObject
{

// Define All Variables Here....

        public $id;
        public $name;
    public static function fromModel(Status $model){
     $data =  [
         'id'=>$model->id,
         'name'=>$model->name,
     ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;

    }


    public static function fromRequest(Request $request){

      $data = [];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;
    }


}
