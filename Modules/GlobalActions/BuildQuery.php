<?php


namespace Modules\GlobalActions;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Modules\Bill\Entities\Bill;
use Spatie\QueryBuilder\QueryBuilder;

class BuildQuery
{


    public function execute($table ,  $model){
        $columns = Schema::getColumnListing($table);
        $result = QueryBuilder::for($model)
            ->allowedSorts($columns)
            ->allowedFilters($columns)
            ->orderByDesc('created_at');
        return $result;

    }

    public function getWithPagination(QueryBuilder $query , $items_per_page){
        return $query->paginate($items_per_page);
    }

    public function getAll(QueryBuilder $query){
        return $query->get();
    }

}
