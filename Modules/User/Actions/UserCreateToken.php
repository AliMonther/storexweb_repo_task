<?php


namespace Modules\User\Actions;


use Modules\User\Entities\User;


class UserCreateToken
{

    public static function execute(User $user){
        return $user->createToken('token')->plainTextToken;
    }
}
