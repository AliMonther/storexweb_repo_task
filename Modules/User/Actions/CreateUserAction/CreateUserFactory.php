<?php

namespace Modules\User\Actions\CreateUserAction;

use Modules\User\DTO\UserDTO\UserData;

class CreateUserFactory
{

       private $class_name;
       private $user_data;

       public function __construct($type , UserData $user_data){

           $this->user_data = $user_data;
           $this->class_name = "Modules\\User\\Actions\\CreateUserAction\\" .$type;
       }

       public function executeFactory(){
            return (new $this->class_name())->execute($this->user_data);
       }
}
