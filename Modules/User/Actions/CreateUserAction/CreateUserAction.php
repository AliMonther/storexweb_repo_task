<?php

namespace Modules\User\Actions\CreateUserAction;

use Modules\User\DTO\UserDTO\UserData;

abstract class CreateUserAction
{
    abstract public function execute(UserData $user_data);
}
