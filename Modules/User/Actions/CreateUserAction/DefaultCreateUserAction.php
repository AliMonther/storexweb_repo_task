<?php

namespace Modules\User\Actions\CreateUserAction;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\User\DTO\UserDTO\UserData;
use Modules\User\Entities\User;

class DefaultCreateUserAction extends CreateUserAction
{


    public function execute(UserData $user_data)
    {
        $arr_data = $user_data->toArray();

            $user = User::create(
                [
                    'phone_number' =>$arr_data['phone_number'],
                    'password' =>Hash::make($arr_data['password']),
                ]
            );

            return $user;

    }


    public function assignRolesToUser(User $user , $roles){

        $user->assignRole($roles);
    }
}
