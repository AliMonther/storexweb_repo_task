<?php

namespace Modules\User\Actions\UserLoginAction;

use Modules\User\Http\Requests\UserLoginRequest;

abstract class UserLoginAction
{
    abstract public function execute(UserLoginRequest $request);
}
