<?php

namespace Modules\User\Actions\UserLoginAction;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Modules\User\Http\Requests\UserLoginRequest;

class DefaultUserLoginAction extends UserLoginAction
{


    public function execute(UserLoginRequest $request)
    {


        $credentials = [
            'phone_number' => $request['phone_number'],
            'password' => $request['password'],
        ];
        $successful_login = Auth::guard('web')->attempt($credentials);
        if($successful_login)
        {
            return User::find(Auth::id());
        }
        return null;


    }
}
