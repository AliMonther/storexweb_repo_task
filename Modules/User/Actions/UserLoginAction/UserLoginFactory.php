<?php

namespace Modules\User\Actions\UserLoginAction;

use Modules\User\Http\Requests\UserLoginRequest;

class UserLoginFactory
{

       private $class_name;
       private $request;

       public function __construct($type , UserLoginRequest $request){

           $this->request = $request;
           $this->class_name = "Modules\\User\\Actions\\UserLoginAction\\" .$type;
       }

       public function executeFactory(){
            return (new $this->class_name())->execute($this->request);
       }
}
