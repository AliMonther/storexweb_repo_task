<?php

namespace Modules\User\Http\Controllers;

use App\DTO\GetResponseData;
use App\DTO\ResponseData;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\User\Actions\CreateUserAction\CreateUserFactory;
use Modules\User\Actions\UserCreateToken;
use Modules\User\Actions\UserLoginAction\UserLoginFactory;
use Modules\User\DTO\UserDTO\UserData;
use Modules\User\Entities\User;
use Modules\User\Http\Requests\UserCreateRequest;
use Modules\User\Http\Requests\UserLoginRequest;

class UserController extends Controller
{


    /**
     * @param UserLoginRequest $request
     * @return ResponseData
     */
    public function login(UserLoginRequest $request){

        $user = (new UserLoginFactory($request->type , $request))->executeFactory();

        if($user){
        $token = UserCreateToken::execute($user);
            return GetResponseData::getResponseData(UserData::fromModel($user,$token) , null , 200);
        }
        return GetResponseData::getResponseData(null ,'authentication field !' ,401);
    }


    public function addNewUser(UserCreateRequest $request){

        $user_data = UserData::fromRequest($request);

        $user = (new CreateUserFactory($request->type , $user_data))->executeFactory();
        if(is_object($user)){
            $token = UserCreateToken::execute($user);
            return GetResponseData::getResponseData(UserData::fromModel($user , $token) , null , 201);
        }
        return GetResponseData::getResponseData(null ,$user  , 422);
    }


    public function deleteUser($user_id){

        User::find($user_id)->delete();
        return GetResponseData::getResponseData(null , 'user deleted successfully' ,200);
    }
}
