<?php

namespace Modules\User\Http\Requests;

use App\PolicyStructure\PolicyFactory;
use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' =>['required','string'],
            'phone_number' =>['required','string','unique:users,phone_number'],
            'password' =>['required','string'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         return (new PolicyFactory('UserCreatePolicy'))->check();
    }
}
