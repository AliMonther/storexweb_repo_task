<?php

declare(strict_types=1);

namespace Modules\User\DTO\UserDTO;

use Spatie\DataTransferObject\Caster;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class  UserCaster implements Caster
{

public function cast(mixed $value): mixed
    {
        return $value->map(/**
         * @throws UnknownProperties
         */
            function(Model $model){
                return DtoData::fromModel($model);
            })->toArray();
    }

}
