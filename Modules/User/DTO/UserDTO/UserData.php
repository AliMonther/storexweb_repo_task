<?php


namespace Modules\User\DTO\UserDTO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\DataTransferObject\DataTransferObject;
use Modules\User\Entities\User;

class UserData extends DataTransferObject
{

        public $id;
        public $token;
        public $phone_number;
        public $password;



    public static function fromModel(User $model,$token){

     $data =  [
             'id'=>$model->id,
             'phone_number'=>$model->phone_number,
             'token'=>$token,
         ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;

    }


    public static function fromRequest(Request $request){

        $from_data = $request->validated();

        $data = [
            'phone_number' => array_key_exists('phone_number',$from_data) ? $from_data['phone_number'] :null,
            'password' => array_key_exists('password',$from_data) ? $from_data['password'] :null,

        ];

            $dto = new static($data);
            $dto->onlyKeys = array_keys($data);

            return $dto;
    }


}
