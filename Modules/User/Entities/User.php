<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
use Modules\Checklist\Entities\Checklist;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory ,softDeletes , HasApiTokens;


    protected $guard_name = 'api';
    protected $fillable = [
        'id',
        'phone_number',
        'email',
        'password'
    ];

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserFactory::new();
    }

    public function checklists(){
        return $this->hasMany(Checklist::class);
    }
}
